import { Component } from '@angular/core';
import { UserService } from './Shared/users.service';
import { CounterService } from './Shared/counter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  activeUsers = [];
  inactiveUsers = [];
  operations = 0;
  /**
   *constructor
   */
  constructor(private userService: UserService, private counterService: CounterService) {
    this.activeUsers = userService.activeUsers;
    this.inactiveUsers = userService.inactiveUsers;
    this.userService.setcounter.subscribe(
      (counter: number) => this.operations = counter
    );
  }
}
