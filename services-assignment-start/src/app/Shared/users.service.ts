import { CounterService } from './counter.service';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class UserService {
    activeUsers = ['Max', 'Anna'];
    inactiveUsers = ['Chris', 'Manu'];

    setcounter =  new EventEmitter<number>();
    /**
     *
     */
    constructor(private counterService: CounterService) {

    }
     SetToInactive(id: number) {
        this.inactiveUsers.push(this.activeUsers[id]);
        this.activeUsers.splice(id, 1);
        this.setcounter.emit(this.counterService.counter ++);
      }

      SetToActive(id: number) {
        this.activeUsers.push(this.inactiveUsers[id]);
        this.inactiveUsers.splice(id, 1);
        this.setcounter.emit(this.counterService.counter ++);
      }
}
