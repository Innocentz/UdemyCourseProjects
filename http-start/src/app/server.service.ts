import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

@Injectable()
export class ServerService {
  constructor(private http: Http) {}

   storeServers(servers: any[]) {
       const headers = new Headers({'Authorization': 'key: AIzaSyAYG6SLHyJLJUKyhZdq6y3bHbUija9dRHc'});
     return this.http.post('https://angularservices-741f4.firebaseio.com/databases/data.json',
     servers,
    {headers: headers}); // this return an Observable and not sending the request yet.
   }
}
