import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { ServersComponent } from './servers/servers.component';
import { ServerComponent } from './servers/server/server.component';
import { EditServerComponent } from './servers/edit-server/edit-server.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthGuard } from './auth-guard.service';
import { CanDeactivateGuard } from './servers/edit-server/can-deactivate-gaurd.service';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ServerResolver } from './servers/server/server-resolver.service';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'users', component: UsersComponent, children: [
      { path: ':id/:name', component: UserComponent }, // loading a component with parameters
    ] },

    {
         path: 'servers',
         // canActivate: [AuthGuard],
         canActivateChild: [AuthGuard],
         component: ServersComponent,
         children: [    // the authGuard protects the route
       { path: ':id', component: ServerComponent, resolve: {server: ServerResolver} },
       { path: ':id/edit', component: EditServerComponent, canDeactivate: [CanDeactivateGuard] }
    ] },
    // { path: 'not-found', component: PageNotFoundComponent },
    { path: 'not-found', component: ErrorPageComponent, data: { message: 'Page not found!'} },
    { path: '**', redirectTo: '/not-found' }, // catch all routes not known and it should be the last by convention
    // { path: 'servers/:id', component: ServerComponent },
    // { path: 'servers/:id/edit', component: EditServerComponent } moved as children of the servers route
  ];

@NgModule({
    imports: [
        // RouterModule.forRoot(appRoutes, {useHash: true})
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule // what needs to be accessible to the module that imports this module
    ]
})
export class AppRoutingModule {

}
