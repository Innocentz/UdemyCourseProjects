import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ServersService } from '../servers.service';

interface Server {
    id: number;
    name: string;
     status: string;
}
// used to resolve data before loading a route, or fetch data
@Injectable()
export class ServerResolver implements Resolve<Server> {
constructor(private ServerService: ServersService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Server> | Promise<Server> | Server {
    return this.ServerService.getServer(+route.params['id']);
  }
}
